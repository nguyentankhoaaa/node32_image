import express from "express";
import { putUserInfor, userInfor, userLogin, userSignUp } from "../controller/userController.js";
import { tokenApi } from "../config/jwt.js";
import upload from "../config/upload.js";



const userRouter = express.Router();
// USER:
//+ Sign Up
userRouter.post("/signup",userSignUp)
// +Login
userRouter.post("/login",userLogin);
// lấy thông tin User
userRouter.get("/get-infor-user",tokenApi,userInfor);
// cập nhật thông tin user
userRouter.put("/put-infor-user",tokenApi,upload.single("file"),putUserInfor)
export default userRouter;
