import express from "express";
import { deleteImageCreate, getImageCreate, getImageSaved, getImageToName, getListImage, postImage} from "../controller/imageController.js";
import { tokenApi } from "../config/jwt.js";
import upload from "../config/upload.js";

const imageRouter = express.Router();
// lấy danh sách ảnh
imageRouter.get("/get-list-image",tokenApi,getListImage)
// lấy danh sách ảnh theo tên
imageRouter.get("/get-image-to-name/:ho_ten",tokenApi,getImageToName);
// lấy danh sách ảnh đã lưu
imageRouter.get("/get-image-saved",tokenApi,getImageSaved);
// lấy danh sách đã tạo 
imageRouter.get("/get-image-create",tokenApi,getImageCreate);
// Xóa ảnh đã tạo
imageRouter.delete("/delete-image-create/:hinh_id",tokenApi,deleteImageCreate);
// post image
imageRouter.post("/post-image",tokenApi,upload.single("file"),postImage)
export default imageRouter  