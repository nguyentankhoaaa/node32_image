import express from "express";
import userRouter from "./userRouter.js";
import imageRouter from "./imageRouter.js";
import inforRouter from "./inforRouter.js";
const rootRouter = express.Router();
rootRouter.use("/user",userRouter);
rootRouter.use("/image",imageRouter)
rootRouter.use("/infor",inforRouter)

export default rootRouter