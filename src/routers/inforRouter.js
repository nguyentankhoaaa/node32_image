import express from "express";
import { getInforComment, getInforImage, getInforSave, postComment } from "../controller/inforController.js";
import { tokenApi } from "../config/jwt.js";

const inforRouter = express.Router();

// lấy thông tin ảnh và người tạo ảnh
inforRouter.get("/get-infor-creator/:hinh_id",tokenApi,getInforImage);
// lấy thông tin bình luận ảnh
inforRouter.get("/get-infor-comment/:hinh_id",tokenApi,getInforComment);
// lấy thông tin đã lưu ảnh chưa 
inforRouter.get("/get-infor-save/:hinh_id",tokenApi,getInforSave);
// Thêm bình luận
inforRouter.post("/post-comment/:nguoi_dung_id/:hinh_id",tokenApi,postComment);
export default inforRouter;