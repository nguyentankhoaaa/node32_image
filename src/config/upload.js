import multer from "multer";


const storage = multer.diskStorage({
    destination: process.cwd() +  "/src/upload/image",
    filename:(req,file,callback)=>{
        let currentTime = new Date();
       let newName = currentTime.getTime() + "_" + file.originalname
         callback(null,newName)
    }
 })


const upload = multer({
    storage
})

export default upload 