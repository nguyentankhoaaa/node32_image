
import jwt from "jsonwebtoken";

let generateToken = (data)=>{
return jwt.sign(data,"IMAGE",{expiresIn:"5m"})
}

let checkToken = (data)=>{
return jwt.verify(data,"IMAGE")
}

let decodeToken = (token)=>{
return jwt.decode(token)
}

let tokenApi = (req,res,next)=>{
    try {
        let {token} = req.headers;
        if(checkToken(token)){
            next()
        }
    } catch (error) {
        res.status(400).send(error.message)
    }
}



export {generateToken,checkToken,decodeToken,tokenApi};