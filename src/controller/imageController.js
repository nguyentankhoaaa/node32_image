import { Sequelize } from "sequelize"
import { successCode,errorCode,failCode } from "../config/response.js"
import initModels from "../models/init-models.js"
import sequelize from "../models/model.js"
import { decodeToken } from "../config/jwt.js"
import fs from "fs"

import config from "../../config/config.js"


let models = initModels(sequelize)
let getListImage =async (req,res)=>{
try {
    let data = await models.hinh_anh.findAll()
    successCode(res,data,"lấy dữ liệu thành công")
} catch  {
    errorCode(res,"error BackEnd")
}
}



let getImageToName = async (req,res)=>{
    try {
        let Op= Sequelize.Op
        let {ho_ten} = req.params;
        let checkName =  await models.nguoi_dung.findOne({where:{ho_ten:{[Op.like]:`%${ho_ten}%`}}});
       if(checkName){
        let data = await models.nguoi_dung.findAll({where:{ho_ten:{[Op.like]:`%${ho_ten}%`}},include:["hinh_anhs"]});
        successCode(res,data,"lấy dữ liệu thành công")
       }else{
        failCode(res,null,"user not exist")
       }
    } catch(error)  {
        errorCode(res,"error BE")
    }
}


let getImageSaved = async(req,res)=>{
    try {
        let {token} = req.headers;
        let newName = decodeToken(token)
    let {nguoi_dung_id} = newName.dataValues
    let data = await models.luu_image.findAll({where:{nguoi_dung_id},include:["hinh"]},);
    successCode(res,data,"lấy dữ liệu thành công")
    } catch  {
        errorCode(res,"error BackEnd")
    }
}

let getImageCreate = async (req,res)=>{
    try {
        let {token} = req.headers;
        let newName = decodeToken(token)
    let {nguoi_dung_id} = newName.dataValues;
 let data = await models.hinh_anh.findAll({where:{nguoi_dung_id}});
successCode(res,data,"lấy dữ liệu thành công")
    } catch  {
        errorCode(res,"error BakEnd")
    }
};

let deleteImageCreate = async(req,res)=>{
    try {
      let {hinh_id} = req.params;
      let checkIMG = await models.hinh_anh.findOne({where:{hinh_id}});
      let checkSaved = await models.luu_image.findOne({where:{hinh_id}})
      if(checkIMG){
        if(checkSaved==null){
            await models.hinh_anh.destroy({where:{hinh_id}});
            successCode(res,"","Xóa dữ liệu thành công")
        }else{
            failCode(res,"","hình ảnh đã lưu : NOT DELETE")
        }
      }else{
        failCode(res,null,"dữ liệu không tồn tại")
      }
    } catch(error)  {
        errorCode(res,"error BackEnd")
    }
}


let postImage =async (req,res)=>{
    try {
        let file = req.file;
        let {token} = req.headers;
        let newName = decodeToken(token)
    let {nguoi_dung_id} = newName.dataValues;
        let {ten_hinh,mo_ta} = req.body;
        fs.readFile(process.cwd()+"/src/upload/image/"+file.filename, (err,data)=>{
            let newName = `data:${file.mimetype};base64,${Buffer.from(data).toString("base64")}` 
            successCode(res,{url:config.url+file.filename,newName},"đăng ảnh thành công")
        })
        let newData = { 
            nguoi_dung_id,
            ten_hinh,mo_ta,
            duong_dan:file.filename
          }
         await models.hinh_anh.create(newData)  
    } catch (error) {
        errorCode(res,error.message)
    }
}

export {getListImage,getImageToName,getImageSaved,getImageCreate,deleteImageCreate,postImage}