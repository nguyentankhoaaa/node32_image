import { errorCode, successCode } from "../config/response.js";
import initModels from "../models/init-models.js";
import sequelize from "../models/model.js";


let models = initModels(sequelize);

let getInforImage =async (req,res)=>{
try {
 
    let {hinh_id}= req.params;
let data = await models.hinh_anh.findAll({include:["nguoi_dung"],where:{hinh_id}});
successCode(res,data,"lấy dữ liệu thành công")
} catch  {
    errorCode(res,"error BackEnd")
}
};
let getInforComment = async(req,res)=>{
try {
    let {hinh_id} = req.params;
    let data = await models.binh_luan.findAll({include:["hinh"],where:{hinh_id}});
    successCode(res,data,"lấy dữ liệu thành công")
} catch  {
    errorCode(res,"error BackEnd")
}
};


let getInforSave = async (req,res)=>{
    try {
        let {hinh_id} = req.params;
        let data = await models.hinh_anh.findAll({include:["luu_images"],where:{hinh_id}});
        successCode(res,data,"lấy dữ liệu thành công")
    } catch(error)  {
        errorCode(res,"error backEnd");
    }
}

let postComment = async (req,res)=>{
    try {
        let {nguoi_dung_id,hinh_id}= req.params;
        let {noi_dung} = req.body;
        let curretTime = new Date();
        let newData = {nguoi_dung_id,hinh_id,ngay_binh_luan:curretTime,noi_dung}
        let data = await models.binh_luan.create(newData) ;
        successCode(res,data,"thêm dữ liệu thành công")
    } catch  {
        errorCode(res,"error BackEnd")
    }
}

export {getInforImage,getInforComment,getInforSave,postComment}