import { where } from "sequelize";
import { decodeToken, generateToken } from "../config/jwt.js";
import { errorCode, failCode, successCode } from "../config/response.js";
import initModels from "../models/init-models.js";
import sequelize from "../models/model.js";
import bcrypt, { compareSync } from "bcrypt"
import config from "../../config/config.js";

let models = initModels(sequelize);


let userSignUp =async (req,res)=>{
try {
    let {ho_ten,tuoi,anh_dai_dien,email,mat_khau}= req.body;
    let checkEmail = await models.nguoi_dung.findOne({where:{email}});
    if(checkEmail== null){
        let newUSer = {ho_ten,tuoi,anh_dai_dien,email,mat_khau:bcrypt.hashSync(mat_khau,10)};
        await models.nguoi_dung.create(newUSer);
        successCode(res,"","sign up success")
    } else{
        failCode(res,null,"email exist")
    }
} catch  {
    errorCode(res,"error BackEnd")
}
}
let userLogin = async (req,res)=>{
    try {
let {email,mat_khau}= req.body;
let checkUser = await models.nguoi_dung.findOne({where:{email}});
if(checkUser){
if(compareSync(mat_khau,checkUser.mat_khau)){
    checkUser={...checkUser,mat_khau:""}
    let token = generateToken(checkUser)
    successCode(res,token,"login success")
}else{
    failCode(res,null,"password wrong")
}
}else{
    failCode(res,null,"email wrong")
}
    } catch  {
        errorCode(res,null,"error backend")
    }
}

let userInfor  = async(req,res)=>{
    try {
        let {token} = req.headers;
        let newName = decodeToken(token)
    let {nguoi_dung_id} = newName.dataValues;
let data = await models.nguoi_dung.findOne({where:{nguoi_dung_id}})

successCode(res,data,"Lấy dữ liệu thành công")
    } catch  {
        errorCode(res,data,"error BackEnd")
    }
}

let putUserInfor = async(req,res)=>{
    try {
        let file = req.file;
        let {gioi_thieu,trang_web,ho_ten,tuoi}= req.body;
        
        let {token} = req.headers;
        let newName = decodeToken(token)
    let {nguoi_dung_id} = newName.dataValues;
        let checkUSer = await models.nguoi_dung.findOne({where:{nguoi_dung_id}});
        console.log(checkUSer)
        if(checkUSer){
            let upData =  {
                trang_web:trang_web,
                ho_ten:ho_ten,
                tuoi:tuoi,
                anh_dai_dien:file.filename,
                gioi_thieu:gioi_thieu}
            await models.nguoi_dung.update(  upData,{where:{nguoi_dung_id}})
            successCode(res,{url:config.url+file.filename},"update success")
        }else{
            failCode(res,"user not exist")
        }
    } catch (error) {
       errorCode(res,error.message) 
    }
}


export {userSignUp,userLogin,userInfor,putUserInfor
}