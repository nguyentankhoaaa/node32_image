import express from "express";
import rootRouter from "./src/routers/rootRouters.js";
const app = express();
app.listen(8080);
app.use(express.json());
app.use(express.static("."))
app.use(rootRouter)